import Phaser from "phaser";

export type MatterBodyConfig = Phaser.Types.Physics.Matter.MatterBodyConfig;