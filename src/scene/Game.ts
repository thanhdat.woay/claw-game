import Phaser from "phaser";
import Ball from "../game-object/Ball";
import Catcher from "../game-object/Catcher";

export default class Game extends Phaser.Scene {
  private cursors!: Phaser.Types.Input.Keyboard.CursorKeys;
  private catcher!: Catcher;
  private balls: Ball[];
  private ballGrabbed!: Ball | undefined;
  public state: "ready" | "up" | "down" = "ready"; // up , down
  // ready => bấm nút: chuyển thành up, gọi hàm onDown
  // onClission => if (state === 'down') => up, gọi hàm onUp
  // update: state === 'up' y < y0: state = 'ready'

  constructor() {
    super("game");
    this.balls = [];
  }

  preload() {
    this.load.image("catcher", "/assets/catcher.png");

    this.load.spritesheet("balls", "/assets/balls.png", {
      frameWidth: 937 / 3,
      frameHeight: 1009 / 3,
    });
  }

  create() {
    this.matter.world.setBounds(0, 0, 500, 700);
    this.cursors = this.input.keyboard.createCursorKeys();

    this.balls = this.createBalls();

    this.catcher = new Catcher(this, 100, 100, "catcher");

    const ballCategory = this.matter.world.nextCategory();
    this.balls.forEach((item) => {
      item.setCollisionCategory(ballCategory);
    });

    this.catcher.setCollidesWith(ballCategory);

    this.catcher.setOnCollide(
      (data: Phaser.Types.Physics.Matter.MatterCollisionData) => {
        if (this.ballGrabbed) return;

        const ball =
          data.bodyA.gameObject.name == "ball"
            ? data.bodyA.gameObject
            : data.bodyB.gameObject;
        this.state = "up";
        this.catcher.onUp();

        ball.grab();
        this.ballGrabbed = ball;
      }
    );
  }

  createBalls() {
    const balls = [];
    const quantity = Phaser.Math.Between(15, 20);

    for (let i = 0; i < quantity; i++) {
      const frame = Phaser.Math.Between(0, 6);
      const ball = new Ball(this, "balls", frame);
      balls.push(ball);
    }

    return balls;
  }

  removeBall(ball: Ball | undefined) {
    if (ball) {
      ball.destroy();
      this.balls = this.balls.filter((item) => item != ball);
      this.ballGrabbed = undefined
    }
  }
  update() {
    if (this.state === "ready") {
      switch (true) {
        case this.cursors.right.isDown:
          this.catcher.moveRight();
          break;
        case this.cursors.left.isDown:
          this.catcher.moveLeft();
          break;
        case this.cursors.space.isDown:
          this.state = "down";
          this.catcher.onDown();
          break;
        default:
          this.catcher.idle();
          break;
      }
    }

    if (
      this.state == "up" &&
      this.catcher.y <= this.catcher.y0 + this.catcher.speed
    ) {
      this.state = "ready";
      this.removeBall(this.ballGrabbed);
    }

    if (
      this.state == "down" &&
      this.catcher.y >= this.cameras.main.height - this.catcher.height / 2
    ) {
      this.state = 'up'
      this.catcher.onUp();
    }

    this.balls.forEach((ball) => ball.update());
  }
}
