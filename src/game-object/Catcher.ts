import Phaser from "phaser";
import Ball from "./Ball";

export default class Catcher extends Phaser.Physics.Matter.Sprite {
  public x0: number;
  public y0: number;

  public speed = 10;

  constructor(scene: Phaser.Scene, x: number, y: number, texture: string) {
    super(scene.matter.world, x, y, texture);

    this.x0 = x;
    this.y0 = y;

    scene.add.existing(this);
    this.scene = scene;
    this.setIgnoreGravity(true);
    this.setAngularVelocity(0);
    this.setBounce(0.2);
  }

  onDown() {
    this.setVelocity(0, this.speed);
  }

  onUp() {
    this.setVelocity(0, -this.speed);
    this.setFixedRotation();
  }

  moveLeft() {
    if ((this.scene as any).state !== "ready") return;
    if (this.x - this.width / 2 <= 0) {
      this.setVelocity(0, 0);
      return;
    }

    this.setVelocity(-this.speed, 0);
  }

  moveRight() {
    if ((this.scene as any).state !== "ready") return;
    if (this.x + this.width / 2 >= this.scene.cameras.main.width) {
      this.setVelocity(0, 0);
      return;
    }
    this.setVelocity(this.speed, 0);
  }

  idle() {
    this.setVelocity(0, 0);
  }
}
