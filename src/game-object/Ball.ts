import Phaser from "phaser";

export default class Ball extends Phaser.Physics.Matter.Sprite {
  public isGrabbing: boolean;
  constructor(scene: Phaser.Scene, texture: string, frame: number) {
    const x = Phaser.Math.Between(50, 450);
    const y = Phaser.Math.Between(500, 600);

    
    super(scene.matter.world, x, y, texture, frame);
    this.scene = scene;
    
    this.name = 'ball'
    scene.add.existing(this);
    // scene.matter.add.gameObject(this);
    this.setBounce(0.5);
    this.setFriction(0.2);
    this.setScale(0.2);
    this.isGrabbing = false;
  }

  grab() {
    this.isGrabbing = true;
    this.x = (this.scene as any)?.catcher.x;
    this.setCollisionCategory(0);
  }

  update() {
    if (this.isGrabbing) {
      this.y =
        (this.scene as any)?.catcher.y +
        (this.scene as any)?.catcher.height / 4;
    }
  }
}
