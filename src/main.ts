import Phaser from "phaser";

import Preload from "./scene/Preload";
import Game from "./scene/Game";

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  parent: "app",
  width: 500,
  height: 700,
  physics: {
    default: "matter",
    matter: {
      debug: true,
      gravity: {
        x: 0,
      },
    },
  },
  scene: [Preload, Game],
};

export default new Phaser.Game(config);
